﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Enums
{
    public enum Status : byte
    {
        Pending = 1,
        Completed = 2,
        InCompleted = 3,
        Canceled = 4,
        Suspended = 5
    }
}
