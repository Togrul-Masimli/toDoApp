﻿using Core.Entities;
using Core.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entites.Concrete
{
    public class ToDoEntity : IEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public int ImportanceLevel { get; set; }
        public Status Status { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
    }
}
