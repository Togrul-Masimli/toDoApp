﻿using Entites.Concrete;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.ValidationRules.FluentValidation
{
    public class ToDoValidator : AbstractValidator<ToDoEntity>
    {
        public ToDoValidator()
        {
            RuleFor(x => x.Title).NotEmpty();
            RuleFor(x => x.Title).MinimumLength(2);
            RuleFor(x => x.Description).NotEmpty();
            RuleFor(x => x.Description).MinimumLength(10);
            RuleFor(x => x.ShortDescription).MinimumLength(5);
            RuleFor(x => x.ImportanceLevel).LessThanOrEqualTo(5);
        }
    }
}
