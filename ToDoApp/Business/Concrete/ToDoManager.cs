﻿using AutoMapper;
using Business.Abstract;
using Business.Constants;
using Business.ValidationRules.FluentValidation;
using Core.Aspects.Autofac.Validation;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entites.Concrete;
using Entites.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Concrete
{
    public class ToDoManager : IToDoService
    {
        private IToDoDal _toDoDal;
        private IMapper _mapper;
        public ToDoManager(IToDoDal toDoDal,IMapper mapper)
        {
            _toDoDal = toDoDal;
            _mapper = mapper;
        }

        [ValidationAspect(typeof(ToDoValidator))]
        public IResult Create(ToDoEntity entity)
        {
            _toDoDal.Create(entity);

            return new SuccessResult(Messages.ToDoAdded);
        }

        public IResult Delete(ToDoEntity entity)
        {
            _toDoDal.Delete(entity);

            return new SuccessResult(Messages.ToDoDeleted);
        }

        public IDataResult<ToDoDto> GetById(int id)
        {
            var result = _toDoDal.Get(x => x.Id == id);

            var mappedResult = _mapper.Map<ToDoDto>(result);

            return new SuccessDataResult<ToDoDto>(mappedResult);
        }

        public IDataResult<ToDoEntity> GetByIdWithId(int id)
        {
            return new SuccessDataResult<ToDoEntity>(_toDoDal.Get(x => x.Id == id));
        }

        public IDataResult<List<ToDoDto>> GetList()
        {
            var result = _toDoDal.GetList();
            var mappedResult = new List<ToDoDto>();

            foreach (var item in result)
            {
                var mapResult = _mapper.Map<ToDoDto>(item);
                mappedResult.Add(mapResult);
            }

            return new SuccessDataResult<List<ToDoDto>>(mappedResult);
        }

        public IDataResult<List<ToDoEntity>> GetListWithId()
        {
            return new SuccessDataResult<List<ToDoEntity>>(_toDoDal.GetList());
        }

        [ValidationAspect(typeof(ToDoValidator))]
        public IResult Update(ToDoEntity entity)
        {
            _toDoDal.Update(entity);

            return new SuccessResult(Messages.ToDoUpdated);
        }
    }
}
