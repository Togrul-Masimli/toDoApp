﻿using Core.Utilities.Results;
using Entites.Concrete;
using Entites.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Abstract
{
    public interface IToDoService
    {
        IDataResult<List<ToDoDto>> GetList();
        IDataResult<ToDoDto> GetById(int id);
        IDataResult<List<ToDoEntity>> GetListWithId();
        IDataResult<ToDoEntity> GetByIdWithId(int id);
        IResult Create(ToDoEntity entity);
        IResult Update(ToDoEntity entity);
        IResult Delete(ToDoEntity entity);
    }
}
