﻿using AutoMapper;
using Core.Enums;
using Entites.Concrete;
using Entites.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Mappers.AutoMapper
{
    public class ToDoProfile : Profile
    {
        public ToDoProfile()
        {
            CreateMap<ToDoEntity, ToDoDto>()
                .ForMember(destination => destination.Status, operation => operation.MapFrom(source => ((Status)source.Status).ToString()))
                .ForMember(destination => destination.CreatedDate, operation => operation.MapFrom(source => source.CreatedDate.ToString("HH:mm:ss")));
        }
    }
}
