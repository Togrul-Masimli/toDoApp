﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Business.Constants
{
    public static class Messages
    {
        public static string ToDoAdded = "ToDo Added Successfully";
        public static string ToDoDeleted = "ToDo Deleted Successfully";
        public static string ToDoUpdated = "ToDo Updated Successfully";
    }
}
