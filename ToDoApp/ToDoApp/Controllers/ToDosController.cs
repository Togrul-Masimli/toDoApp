﻿using AutoMapper;
using Business.Abstract;
using Entites.Concrete;
using Entites.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToDoApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ToDosController : ControllerBase
    {
        private IToDoService _toDoService;
        public ToDosController(IToDoService toDoService)
        {
            _toDoService = toDoService;
        }

        [HttpGet("getlist")]
        public IActionResult GetList()
        {
            var result = _toDoService.GetList();
            if (result.Success)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        // Maybe Required
        [HttpGet("getlistwithid")]
        public IActionResult GetListWithId()
        {
            var result = _toDoService.GetListWithId();
            if (result.Success)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [HttpGet("getbyid")]
        public IActionResult GetById(int id)
        {
            var result = _toDoService.GetById(id);
            if (result.Success)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        // Maybe Required
        [HttpGet("getbyidwithid")]
        public IActionResult GetByIdWithId(int id)
        {
            var result = _toDoService.GetById(id);
            if (result.Success)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [HttpPost("create")]
        public IActionResult Create(ToDoEntity toDo)
        {
            var result = _toDoService.Create(toDo);
            if (result.Success)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [HttpPut("update")]
        public IActionResult Update(ToDoEntity toDo)
        {
            var result = _toDoService.Update(toDo);
            if (result.Success)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [HttpDelete("delete")]
        public IActionResult Delete(ToDoEntity toDo)
        {
            var result = _toDoService.Delete(toDo);
            if (result.Success)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }
    }
}
